package com.example.customStack;

import java.util.EmptyStackException;

public class CustomStack<T> {
    private int size;
    private Node top;

    /**
     * Add object at the end of a stack
     *
     * @param item object to be added
     */
    public void add(T item) {
        Node node = new Node(item);
        node.previous = top;
        top = node;
        size++;
    }

    /**
     * Remove object from a stack, object removed is always last added item (LIFO)
     */
    public T remove() {
        if (size == 0) {
            return null;
        }
        Node removed = top;
        if (size > 0) {
            top = top.previous;
            size--;
        }
        return removed.item;
    }

    /**
     * Remove object from specific index
     *
     * @param index stack's index from which user wants to remove object
     */
    public T remove(int index) {
        Node removed = top;
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        } else if (index == size - 1) {
            top = top.previous;
            size--;
        } else {
            int tempIndex = size - 2;
            Node temp = top;
            while (tempIndex != index) {
                tempIndex--;
                temp = temp.previous;
            }
            size--;
            removed = temp.previous;
            temp.previous = temp.previous.previous;
        }
        return removed.item;
    }

    /**
     * Remove specific object from stack
     *
     * @param item object which user wants to remove
     */
    public boolean remove(T item) {
        if (size == 0) {
            return false;
        }
        if (top.item == item) {
            top = top.previous;
            size--;
            return true;
        } else {
            int tempIndex = size - 2;
            Node temp = top;
            while (temp.previous != null) {
                if (temp.previous.item == item) {
                    temp.previous = temp.previous.previous;
                    size--;
                    return true;
                }
                temp = temp.previous;
                tempIndex--;
            }
        }
        return false;
    }

    /**
     * Check if specific object is already in a stack
     *
     * @param item object to be checked if already added to the stack
     */
    public boolean contains(T item) {
        if (size == 0) {
            return false;
        }
        Node temp = top;
        while (temp != null) {
            if (temp.item == item) {
                return true;
            }
            temp = temp.previous;
        }
        return false;
    }

    /**
     * Get bottom of a stack (last item that will be deleted)
     */
    public T bottom() {
        Node temp = top;
        if (size == 0) {
            return null;
        }
        while (temp.previous != null) {
            temp = temp.previous;
        }
        return temp.item;
    }

    /**
     * Get peek of a stack (first item that will be deleted)
     */
    public T peek() {
        if (size == 0) {
            throw new EmptyStackException();
        }
        return top.item;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Stack [");
        if (top == null) {
            return result.append("] size: ").append(" size: " + size).toString();
        } else {
            Node temp = top;
            while (temp.previous != null) {
                result.append(temp).append(" --> ");
                temp = temp.previous;
            }
            result.append(temp);
        }
        return result + "]" + " size: " + size;
    }

    private class Node {
        private final T item;
        private Node previous;

        private Node(T item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return item.toString();
        }
    }
}
