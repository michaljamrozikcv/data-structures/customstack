package com.example.customStack.elements;

public class Car {
    private final String model;
    private final String vin;

    public Car(String model, String vin) {
        this.model = model;
        this.vin = vin;
    }

    @Override
    public String toString() {
        return "model: "+ model+", VIN: "+  vin;
    }
}
