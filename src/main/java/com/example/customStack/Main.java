package com.example.customStack;

import com.example.customStack.elements.Car;
import com.example.customStack.elements.Person;

public class Main {
    public static void main(String[] args) {
        stackOfPeople();
        stackOfCars();
    }

    private static void stackOfPeople() {
        System.out.println("\n=========================================================================");
        System.out.println("STACK OF PEOPLE");
        System.out.println("=========================================================================");
        Person p1 = new Person("name1", "surname1");
        Person p2 = new Person("name2", "surname2");
        Person p3 = new Person("name3", "surname3");
        Person p4 = new Person("name4", "surname4");
        Person p5 = new Person("name5", "surname5");
        CustomStack<Person> stack = new CustomStack<>();
        System.out.println("\n----------------------Adding to stack-------------------");
        stack.add(p1);
        stack.add(p2);
        stack.add(p3);
        stack.add(p4);
        stack.add(p5);
        System.out.println(stack);
        System.out.println("\n------------------------Peek------------------------");
        System.out.println(stack.peek());
        System.out.println("\n------------------------Bottom------------------------");
        System.out.println(stack.bottom());
        System.out.println("\n------------------------Removing from the top------------------------");
        stack.remove();
        System.out.println(stack);
        int index = 2;
        System.out.println("\n------------------------Removing by index " + index+"------------------------");
        System.out.println("item removed: " + stack.remove(index));
        System.out.println(stack);
        System.out.println("\n------------------------Removing by item------------------------");
        System.out.println("Does element " + p5 + " has been removed? " + stack.remove(p5));
        System.out.println("Does element " + p1 + " has been removed? " + stack.remove(p1));
        System.out.println(stack);
    }

    private static void stackOfCars() {
        System.out.println("\n=========================================================================");
        System.out.println("STACK OF CARS");
        System.out.println("=========================================================================");
        Car p1 = new Car("model1", "vin1");
        Car p2 = new Car("model2", "vin2");
        Car p3 = new Car("model3", "vin3");
        Car p4 = new Car("model4", "vin4");
        Car p5 = new Car("model5", "vin5");
        CustomStack<Car> stack = new CustomStack<>();
        System.out.println("\n----------------------Adding to stack-------------------");
        stack.add(p1);
        stack.add(p2);
        stack.add(p3);
        stack.add(p4);
        stack.add(p5);
        System.out.println(stack);
        System.out.println("\n------------------------Peek------------------------");
        System.out.println(stack.peek());
        System.out.println("\n------------------------Bottom------------------------");
        System.out.println(stack.bottom());
        System.out.println("\n------------------------Removing from the top------------------------");
        stack.remove();
        System.out.println(stack);
        int index = 2;
        System.out.println("\n------------------------Removing by index " + index+"------------------------");
        System.out.println("item removed: " + stack.remove(index));
        System.out.println(stack);
        System.out.println("\n------------------------Removing by item------------------------");
        System.out.println("Does element " + p5 + " has been removed? " + stack.remove(p5));
        System.out.println("Does element " + p1 + " has been removed? " + stack.remove(p1));
        System.out.println(stack);
    }
}
